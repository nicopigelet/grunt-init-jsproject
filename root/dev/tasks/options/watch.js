
module.exports = { 
	gruntfile: {
		files: '<%= jshint.gruntfile.src %>',
		tasks: ['jshint:gruntfile']
	},
	test: {
		files: ['dev/src/**/*.js', 'dev/sass/**/*.scss'],
		tasks: 'default'
	}
}