/*
 * grunt-init-jsproject
 *
 * Copyright (c) 2013 Nicolas Pigelet, contributors
 * Licensed under the MIT license.
 */

'use strict';

var exec = require('child_process').exec, 
    child;

// Basic template description.
exports.description = 'Create a base js project';

// Template-specific notes to be displayed before question prompts.
exports.notes = 'Project made by Sennep.';

// Template-specific notes to be displayed after question prompts.
// exports.after = 'The bash should now install all the npm dependencies';

// Any existing file or directory matching this wildcard will cause a warning.
exports.warnOn = '*';

// The actual init template.
exports.template = function(grunt, init, done) {

  init.process({type: 'jsproject'}, [
    
    // Prompt for these values.
    init.prompt('name'),
    init.prompt('title'),
    {
        name: 'namespace',
        message: 'Namespace of the project',
        default: 'NS', 
        validator: /^[A-Z]{0,9}$/,
        warning: 'Must be only letters, max 9'
    },
    init.prompt('description'),
    init.prompt('version'),
    init.prompt('repository'),
    init.prompt('homepage'),
    init.prompt('bugs'),
    init.prompt('licenses', 'MIT'),
    init.prompt('author_name'),
    init.prompt('author_email'),
    init.prompt('author_url'),
    init.prompt('jquery_version', '1.10.2')

  ], function(err, props) {

    props.keywords = [];
    
    // Files to copy (and process).
    var files = init.filesToCopy(props);

    // Add properly-named license files.
    init.addLicenseFiles(files, props.licenses);

    // Actually copy (and process) files.
    init.copyAndProcess(files, props);

    // Generate package.json file.
    init.writePackageJSON('package.json', props, function( pkg, props ){

        pkg.namespace = props.namespace;
        pkg.jquery_version = props.jquery_version;

        return pkg;
    });
    
    // Install all the npm modules necessary
    console.log("Installing npm modules...");

    var 
        devDependencies = [
            "glob",
            "load-grunt-tasks",
            "grunt",
            "grunt-contrib-watch", 
            "grunt-contrib-concat", 
            "grunt-contrib-uglify", 
            "grunt-contrib-jshint",
            "grunt-contrib-sass"
        ],
        npmInstall = "npm install --save-dev " + devDependencies.join(" ");

    exec(npmInstall, function(error,stdout,stderr) {
        if (error !== null) {
            console.log('exec error: ' + error);
        }
        done();
    });

  });

};
